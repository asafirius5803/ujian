-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 20, 2019 at 09:29 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.39

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ujian`
--

-- --------------------------------------------------------

--
-- Table structure for table `dusun`
--

CREATE TABLE `dusun` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `alamat` varchar(30) NOT NULL,
  `pekerjaan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dusun`
--

INSERT INTO `dusun` (`id`, `nama`, `alamat`, `pekerjaan`) VALUES
(1, 'saya', 'jauh', 'nganggur'),
(2, 'dia', 'kota jogja', 'wiraswasta');

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE `nilai` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `nilai_bahasa_indonesia` varchar(15) NOT NULL,
  `nilai_matematika` varchar(15) NOT NULL,
  `nilai_ipa` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `simpanjawab`
--

CREATE TABLE `simpanjawab` (
  `1` varchar(5) NOT NULL,
  `2` varchar(5) NOT NULL,
  `3` varchar(5) NOT NULL,
  `4` varchar(5) NOT NULL,
  `5` varchar(5) NOT NULL,
  `6` varchar(5) NOT NULL,
  `7` varchar(5) NOT NULL,
  `8` varchar(5) NOT NULL,
  `9` varchar(5) NOT NULL,
  `10` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_soalbhsindo1`
--

CREATE TABLE `tbl_soalbhsindo1` (
  `id_soal` int(5) NOT NULL,
  `soal` text NOT NULL,
  `a` varchar(50) NOT NULL,
  `b` varchar(50) NOT NULL,
  `c` varchar(50) NOT NULL,
  `d` varchar(50) NOT NULL,
  `knc_jawaban` varchar(30) NOT NULL,
  `aktif` varchar(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_soalbhsindo1`
--

INSERT INTO `tbl_soalbhsindo1` (`id_soal`, `soal`, `a`, `b`, `c`, `d`, `knc_jawaban`, `aktif`) VALUES
(1, 'Bacalah kutipan karya tulis berikut ini! Manusia dalam kehidupannya tidak dapat melepaskan diri dari teknologi, baik yang sederhana maupun yang mutakhir. Pada era modernisasi, teknologi memberikan sumbangan yang begitu besar. Namun, banyak pula akibat buruk yang diperoleh dari dampak teknologi. Oleh karena itu, perlu diciptakan teknologi yang manusiawi, yaitu teknologi yang mampu mengembangkan harkat dan martabat manusia. Dalam karya tulis ilmiah, uraian di atas merupakan bagian?', 'Tujuan penulisan', ' Landasan teori', 'Latar belakang masalah', 'Pembatasan masalah', 'c', 'Y'),
(2, 'Mereka selalu saja bermusuhan tidak pernah rukun. Sekarang mereka harus menanggung segala akibat dari perbuatannya. Gurindam yang sesuai dengan ilustrasi di atas adalah...', 'Kurang pikir kurang siasat', ' Siapa menggemari silang-sengka', 'Kalau diri kena perkara', 'Fikir dahulu sebelum berkata', 'b', 'Y'),
(3, 'Bacalah larik-larik puisi berikut! Buah mengkudu kusangka kandis Kandis terletak dalam puan Gula madu kusangka manis... Larik yang tepat untuk melengkapi pantun di atas adalah..', 'Senyum adinda memang manis ', 'Gula manis di dalam cawan', 'Kawan manis idaman hati', 'Manis lagi senyummu, Tuan', 'd', 'Y'),
(4, 'Bacalah surat undangan berikut ini! Yth. Saudara Bambang Sukadi Jalan Sidobali 25 Kota Gede, Yogyakarta hari : Senin tanggal : 20 Agustus 2004 waktu : pukul 08.30 sampai selesai tempat : ruang sidang SMA Pertiwi Kami mengucapkan terima kasih atas perhatian saudara. Kalimat yang tepat untuk melengkapi bagian pembuka surat di atas adalah ..', 'Bersama surat ini kami mengund', 'Dengan surat ini kami mengunda', 'Kami mengundang Saudara untuk', 'Kami mengundang Saudara hadir', 'b', 'Y'),
(5, 'Penulisan kata bergaris bawah berikut ini yang tidak tepat terdapat pada kalimat?', 'Warga komplek membangun gedung', 'Pada saat pasca panen, harga g', 'Setiap bus antarpropinsi harus', ' Para tunawisma di jembatan la', 'b', 'Y'),
(6, 'Bacalah penggalan puisi berikut dengan saksama! Teratai Kepada Ki Hajar Dewantara Dalam kebun di tanah airku. Tersembunyi kembang indah permai Tidak terlihat orang yang lalu Akarnya tumbuh di hati dunia Daun bersemi Laksmi mengarang Biarpun ia diabaikan orang Seroja kembang gemilang mulia (Sanusi Pane) Majas alegori yang tepat untuk melengkapi puisi diatas adalah...', 'Tumbuh sekuntum bunga teratai', 'Lahirlah bunga bangsaku', 'Bunga bangsa berguguran', 'Ditanami bunga seroja', 'a', 'Y'),
(7, 'Mudah mencampuri urusan orang lain merupakan perbuatan tidak terpuji. Dan kita akan tersisih apabila orang lain tersebut bersanak saudara. Peribahsa yang tepat sesuai dengan pernyataan diatas adalah...', 'Air sama air kelak menjadi sat', ' Seperti biduk dikayuh hilir', 'Kucing lalu, tikus tidak berde', 'Terkusut-kusut sebagai anak ti', 'a', 'Y'),
(8, '(1) Dapat pula dikemukakan bahwa dalam paragraf yang kohesif tidak terdapat kalimat yang saling bertentangan. (2) Kohesif bermakna kepaduan. (3) Paragraf yang kohesif adalah paragraf yang hubungan antar kalimatnya padu atau berjalinan erat. (4) Kepaduan itu ditandai dengan terciptanya saling mendukung antara kalimat yang satu dengan kalimat yang lainnya. (5) Lebih jelas lagi dapat dikatakan bahwa paragaraf yang kohesif ditandai dengan tidak terjadinya saling mengingkari antara kalimat satu dengan kalimat lainnya. Kalimat-kalimat tersebut akan menjadi paragraf yang padu apabila disusun dengan urutan...', ' (2), (3), (5), (4), (1)', '(1), (3), (5), (4), (2)', ' (5), (3), (2), (4), (1)', '(2), (3), (4), (5), (1)', 'd', 'Y'),
(9, '(1) Dengarkan angin mengusir batang-batang padi. (2) Sebelum matahari terbenam. (3) Dengarkan senandung di balik jendela. (4) Rumah kecil, bukanlah pintu pagarku (5) Angin datang mengantarkan berita Berdasarkan puisi tersebut larik yang bermajas sama dintadai dengan nomor...', '(1) dan (2)', '(1) dan (3)', '(1) dan (2)', '(1) dan (5)', 'd', 'Y'),
(10, 'Semburan Baru Muncul di Mindi Semburan lumpur, air, dan gas baru keluar dari halaman belakang rumah salah seorang penduduk, warga Desa Mindi, Kecamatan Porong, Kabupaten Sidoarjo. Semburan itu merupakan semburan ke-59 yang muncul di sekitar pusat semburan utama. Menurut seorang ahli dan Leader Team Fergaco, perusahaan yang mengawasi gas-gas berbahaya di sekitar pusat semburan, semburan itu sama dengan 58 semburan liar sebelumnya. Semburan liar itu juga tidak berbahaya dan tidak akan membesar. Kalau dibiarkan semburan itu akan mengecil sendiri. Untuk menutup semburan, hari ini akan dimasukkan 100 kilogram semen ke dalam lubang asal semburan. Ide pokok paragraf kedua teks tersebut yang tepat adalah ....', 'Pengawasan gas oleh tim ahli.', 'Pendapat tentang semburan liar', 'Munculnya semburan liar', 'Mengecilnya semburan liar', 'c', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_soalbhsindo2`
--

CREATE TABLE `tbl_soalbhsindo2` (
  `id_soal` int(5) NOT NULL,
  `soal` text NOT NULL,
  `a` varchar(30) NOT NULL,
  `b` varchar(30) NOT NULL,
  `c` varchar(30) NOT NULL,
  `d` varchar(30) NOT NULL,
  `knc_jawaban` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_soalbhsindo2`
--

INSERT INTO `tbl_soalbhsindo2` (`id_soal`, `soal`, `a`, `b`, `c`, `d`, `knc_jawaban`) VALUES
(1, 'Selain unsur-unsur intrinsik drama, unsur lain yang penting untuk diperhatikan dalam pementasan adalah..', 'penonton', 'pentas', 'tiket masuk', ' daya tarik tampung gedung', 'b'),
(2, 'Angin bertiup sepoi-sepou. Cuaca begitu cerah cemerlang terkena sinar rembulan. Bintang-bintang bertaburan di atas langit seperti permata bertebaran di atas permadani. Di sebelah sana melancarlah biduk para nelayan yang sedang mengadu nasib dan untung, menentang besarnya gelombang yang penuh dengan marabahaya. Latar dari kutipan novel tersebut ialah...', 'Malam hari di langit biru', ' Malam hari di laut lepas', ' Pagi hari di samudera raya', ' Siang hari di tengah laut', 'b'),
(3, 'Teks 1: Badan Pengkajian dan Penerapan Teknologi (BPPT) mengemukakan teknologi reusable sanitary landfill. Teknologi itu ialah teknologi pengolahan sampah sanitary landfill khusus untuk sampah basah. Teknologi tersebut diharapkan mampu menghilangkan polusi udara. \r\n Teks 2: Sampah yang berasal dari rumah tangga setidaknya menyumbang sekitar 6000 ton total dari produksi sampah setiap harinya di ibukota Jakarta. Apabila setiap rumah tangga bisa mengelola sampah dengan baik, maka diharapkan dapat membantu mengatasi masalah sampah. Oleh karenanya, BPPT menggalakkan pengolahan sampah rumah tangga sehingga menjadi kompos yang bermanfaat untuk tumbuhan di dalam rumah. \r\n Persamaan informasi yang dibahas pada kedua teks tersebut ialah...', ' Sampah dan pengolahannya ', 'Produksi sampah dan teknologi', ' Jakarta dan sampah', 'BPPT dan sampah', 'd'),
(4, 'Saudara-saudaraku, rasa optimisme untuk bersatu sangat dibutuhkan oleh bangsa kita tidak bisa memungkiri, disana sini kegagalan mental banyak ditemukan. Ragam penipuan, korupsi, kolusi, nepotisme bertebaran. Nah, apakah dengan kenyataan ini para pemuda kita akan tetap diam? Menuggu bom waktu meledak karena kesalahan bangsa sendiri? Gagasan utama pidato diatas adalah...', 'makin marak ragam penipuan', 'bangsa tengah menghadapi kemun', 'menanti bom waktu', 'ajaran untuk optimis', 'b'),
(5, 'Banyak iklan yang menawarkan jasa untuk menurunkan berat badan. Ada yang melalui tusuk jarum, sedot lemak, minum jamu, atau minum teh hijau. Jenis diet pun bermacam-macam. Ada diet macan yang lebih mementingkan konsumsi daging. Ada diet buah-buahan membatasi makan nasi dan makanan berkarbohidrat tinggi. Beragam kiat dapat dilakukan untuk menurunkan berat badan.Simpulan yang tepat untuk melengkapi paragraf tersebut adalah ....', 'Kita tidak akan mengalami kesu', 'Kaum wanita berlomba-lomba mel', 'Para remaja di kota-kota besar', 'Banyaknya iklan penawaran diet', 'd'),
(6, 'Rindu Dendam Aku mengembara seorang diri Antara bekas Majapahit Aku bermimpi, terkenang dulu Dan teringat waktu sekarang O, Dewata, pabila gerang Akan kembali kemegahan Dan keindahan tanah airku? Sanusi Pane Amanat yang ada pada puisi tersebut ialah.. ', 'Hidup harus menjadi seorang pe', 'Sudah selayaknya kita membaca', 'Sukses adalah pilihan', 'Kita perlu belajar dengan kesu', 'd'),
(7, '(1) Objek wisata Pangandaran menyediakan transportasi rekreasi untuk memudahkan wisatawan menikmati keindahan pantai. (2) Wisatawan domestik maupun mancanegara dapat menggunakan transportasi untuk menikmati keindahan alam. (3) Di sepanjang tepi Pantai Pangandaran terlihat berjejer perahu untuk disewakan. (4) Dengan biaya Rp 1.000,00 saja per orang, para wisatawan dapat berputar di sekitar pantai dan menikmati keindahaan taman laut. (5) Para pedagang ikut meramaikan situasi pantai Pangandaran. Kalimat utama paragraf tersebut terdapat pada nomor ...', '(1)', '(2)', '(3)', '(4)', 'a'),
(8, '(1) Dalam upaya pencegahan pencemaran udara, hutan mampu menangkal polutan gas ataupun butiran padat. (2) Hasil penelitian menunjukkan bahwa volume udara yang mengandung polusi gas zon sebesar 150 ppm gas ternyata 99% terserap oleh tegukan hutan dalam waktu delapan jam. (3) Komplek industri yang mengeluarkan polutan belerang dioksida di Uni Rusia ternyata berkurang dengan adanya jalur vegetasi kayu selebar 500 m yang mengelilingi kawasan industri tersebut. (4) Tumbuhan berkayu ataupun pohon memang diandalkan dalam penyelamatan keadaan lingkungan seperti tanah, air, dan udara walaupun peran pohon tersebut sebatas pada lingkungan, yang belum akut. (5) Pohon memang tidak akan mampu menetralisasi polusi, terutama pada kawasan industri besar. Kalimat yang berisi fakta terdapat pada nomor?', '(1) dan (2)', '(1) dan (5)', '(2) dan (3)', '(3) dan (4)', 'c'),
(9, 'Cara untuk menarik untuk mengawali pidato adalah...', 'mengungkapkan tema pidato', 'menjelaskan urutan sistematika', ' mengungkapkan fakta yang berh', 'menjelaskan hal-hal yang berka', 'c'),
(10, 'Sepak bola adalah cabang olah raga yang telah merakyat di kalangan masyarakat. Permainan olah raga tersebut telah dikenal oleh setiap orang. Sepak bola bukan hanya kegemaran kaum lelaki, kaum wanita juga banyak yang menggemari olah raga ini. Ide pokok dari kutipan paragraf tersebut ialah...', 'Olahraga sepak bola', 'Kegemaran laki-laki', 'Keikutsertaan wanita', ' Sepak bola olah raga dunia', 'a');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_soalbhsindo3`
--

CREATE TABLE `tbl_soalbhsindo3` (
  `id_soal` int(5) NOT NULL,
  `soal` text NOT NULL,
  `a` varchar(30) NOT NULL,
  `b` varchar(30) NOT NULL,
  `c` varchar(30) NOT NULL,
  `d` varchar(30) NOT NULL,
  `knc_jawaban` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_soalbhsindo3`
--

INSERT INTO `tbl_soalbhsindo3` (`id_soal`, `soal`, `a`, `b`, `c`, `d`, `knc_jawaban`) VALUES
(1, 'Hubungan antara esai dengan artikel adalah...', 'menggunakan bahasa yang inform', 'menggunakan pendapat dan panda', 'menggunakan bahasa dengan gaya', 'tulisan sama-sama tepat sasara', 'b'),
(2, '(1) Ketika itu pula ibu menceritakan bahwa kakanya Narothama lulus sebagai letnan muda dan dua minggu lagi akan diwisuda. (2). Suasana keluarga menjadi ceria mendengar kabar itu. Sambil bekerja tak henti-hentinya mereka memebicarakan Narothama. (3). Santi mewaili keluargana untuk menghadiri wisuda. (4). Malam keberangkatan santi tidak dapat tidur nyenak, banyak yang direncanakan dan yang diharapkannya. Bukti suasana yang harap-harap cemas dan gelisah pada kutiban cerpen tersebut adalah pada kalimat nomor...', '(4)', '(3)', '(2)', '(1)', 'a'),
(3, 'Mereka melaksanakan daripada kegiatan rutin yang sering dilaksanakan setiap tahun. Bagian kalimat yang menyebabkan ketidak efektifan kalimat tersebut adalah...', 'mereka, daripada ', ' kegiatan, selalu', ' daripada, sering', 'daripada, yang', 'c'),
(4, 'Dadi, Darmo dan Linus, orag-orang yang paling dekat di hati mimin, kini sedang dikejar-kejar oleh penjahat. Pemerintah bukan lagi memimpin dan pelindung baginya. Tapi semata mata penguasa. Penguasa yang kejam. Konflik yang dialami teman-teman Mimin dalam kutiban novel tersebut adalah konflik...', 'batin', 'psikis', 'fisik', 'ide', 'c'),
(5, 'Saya menolak atau lebih tepatnya tidak menerima penuh bahwa puisi mesti padat, harus sedikit kata-kata. Daripada memenuhi syarat padat dan minimum tapi tidak indah serta gagap komunikasi, saya memilih puisi banyak kata tapi cantik, menyentuh, laju menghilir dan komunikatif. Puisi saya wajib musical. Kata-kata harus sedap didengar. Tentu saja kata-kata itu mengalami ketatnya seleksi. Kutipan esai diatas termasuk jenis...', 'reflektif', 'argumentatif', 'proses', 'sejarah', 'b'),
(6, 'Tubuh biru Tatapan mata biru Lelaki terguling di jalan Lewat gardu belanda dengan bumi Berlindung warna malam Sendiri masuk kota Ingin ikut ngubur ibunya Tema puisi tersebut adalah...', 'perlawanan', 'keberanian', 'kekejaman', 'perjuangan', 'd'),
(7, 'Daerah yang memiliki luasa 1.119,44 kilometer persegi dengan penduduk tak lebih dari satu jiwa ini sebenarnya memiliki potensi yang menarik. Selain memiliki pantai yang inda, Pacitan juga memiliki gua paling menarik di Asia Tenggara. Tulisan di atas bukan esai, karena...', 'berisi pendapat penulis', 'lebih mengutamakan proses anal', ' mengarah kepada proses', 'mementingkan unsur berita', 'd'),
(8, 'Berikut ini merupakan jenis esai, yaitu...', 'proses', 'keagamaan', 'pendapat', 'perjalanan', 'c'),
(9, 'Bila kasih mu ibarat samudra Sempit lautan tuduh Tempat ku mandi, mencuci lumut pada diri Tempat ku berlayar, menebar pukat dan melempar sauh Kalau aku ujian kemudian ditanya tentang pahlawan Nama ku yang ku sebut paling dahulu Lantaran ku tahu Engkau ibu dan aku anak mu. \" Ibu \" karya D Zawawi Imron. Isi puisi tersebut menggambarkan...', 'Kesedihan seorang anak', 'Kegelisahan hati seorang anak', 'Perasaan rindu sreorang anak', 'Perasaan sayang seorang anak', 'd'),
(10, 'Dalam buku Sri Sumarah (sebenarnya nama judul salah satu cerpen yang ada didalam buku tersebut), Umar Kayam seperti ingin menguak dunia yang kerap dihadapi oleh beberapa perempuan dalam berbagai situasi. Dengan sendirinya pengarang menciptakan karakter dan dunia keperempuanan pada suatui masa secara cerdas. Hal yang dibahas dalam esai di atas adalah...', 'tema', 'amanat', 'latar', 'tokoh', 'd');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_soalipa1`
--

CREATE TABLE `tbl_soalipa1` (
  `id_soal` int(5) NOT NULL,
  `soal` text NOT NULL,
  `a` varchar(30) NOT NULL,
  `b` varchar(30) NOT NULL,
  `c` varchar(30) NOT NULL,
  `d` varchar(30) NOT NULL,
  `knc_jawaban` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_soalipa1`
--

INSERT INTO `tbl_soalipa1` (`id_soal`, `soal`, `a`, `b`, `c`, `d`, `knc_jawaban`) VALUES
(1, 'Teratai dapat terapung di permukaan air karena mmepunyai...', 'Batang yang berongga', 'Akar yang panjang', 'Daun yang kecil', ' Duri yang tajam', 'a'),
(2, 'Fungsi paruh bebek yang lebar dan tipis yaitu...', 'Mengoyak daging', 'Meminum air sebanyak-banyaknya', 'Menyaring makanan dari air', 'Menggaruk tanah', 'c'),
(3, ' Bebek melakukan adaptasi berupa...', 'Memiliki bulu yang tipis ', 'Mempunyai paruh yang tajam', ' Memiliki selaput pada kaki', ' Memiliki cakar pada kaki', 'c'),
(4, 'Beruang kutub dapat terlindung dari cuaca dingin karena mempunyai..', 'Bulu yang keras dan runcing', ' Lapisan lemak dan bulu tebal', ' Darah yang panas', 'Lapisan kulit yang tajam', 'b'),
(5, 'Contoh tumbuhan yang melakukan adaptasi untuk memperoleh makanan yaitu..', ' Kantung semar', ' Mawar', 'Kaktus', 'Teratai', 'a'),
(6, 'Kemampuan bunglon mengubah warna tubuhnya yaitu untuk...', ' Memudahkan berkembangbiak', 'Memudahkan mengendap menangkap', ' Menarik perhatian mangsa', ' Memudahkan memanjat pohon', 'a'),
(7, 'Unta memiliki punuk dipunggungnya yang berguna untuk...', ' Menarik perhatian betina', 'Menyimpan cadangan makanan dan', ' Melindungi diri dari musuh', 'Menarik penampilan di gurun', 'b'),
(8, ' Hewan yang melindungi diri dengan mengeluarkan bau yang menyengat yaitu..', 'Walangsangit', 'Gajah', 'Komodo', 'Jangkrik', 'a'),
(9, ' Hewan yang melumpuhkan mangsa dengan racun yang dimilikinya yaitu', 'lipan dan musang', 'macan dan kalajengking', 'ular dan singa', 'ular dan kalajengking', 'd'),
(10, 'Landak melindungi diri dengan cara..', 'Sirip yang luas', 'Kulit duri yang tajam', 'Bau yang menyengat', 'Racun yang berbisa', 'b');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_soalipa2`
--

CREATE TABLE `tbl_soalipa2` (
  `id_soal` int(5) NOT NULL,
  `soal` text NOT NULL,
  `a` varchar(30) NOT NULL,
  `b` varchar(30) NOT NULL,
  `c` varchar(30) NOT NULL,
  `d` varchar(30) NOT NULL,
  `knc_jawaban` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_soalipa2`
--

INSERT INTO `tbl_soalipa2` (`id_soal`, `soal`, `a`, `b`, `c`, `d`, `knc_jawaban`) VALUES
(1, ' Pohon jati mengggugurkan daunnya dengan tujuan ....', 'Mempercepat pertumbuhan batang', 'Mengurangi penguapan', 'Menarik pemangsa', 'Meneduhkan daunnya', 'b'),
(2, 'Penyesuaian diri makhluk hidup terhadap lingkungan disebut....', 'Adaptasi', 'Habitat', 'Organisasi', 'Ekosistem', 'a'),
(3, 'Bentuk paruh yang dimiliki burung menyesuaikan ....', 'Jenis sarangnya ', ' Jenis bulunya', 'Jenis telurnya', 'Jenis makanannya', 'd'),
(4, 'Cicak memutuskan ekornya dengan tujuan ....', 'Menarik cicak betina', 'Menarik perhatian mangsanya', ' Mengelabuhi pemangsanya', 'Mudah berkembangbiak', 'c'),
(5, 'Bentuk taring dan cakar singa yang tajam bermanfaat untuk ....', 'Memanjat pohon', 'Berenang', 'Menggali tanah', 'Mencengkram mangsa', 'd'),
(6, 'Kemampuan bunglon merubah warna tubuhnya dinamakan ....', 'Reboisasi', 'Ekolokasi', 'Autotomi', 'Mimikri', 'd'),
(7, 'Menggugurkan daun ketika kemarau merupakan bentuk adaptasi dari tanaman berikut ini, kecuali ....', 'Randu', ' Kantung semar', 'Jati', 'Pohon Mahoni', 'b'),
(8, 'Di bawah ini ayang termasuk ciri-ciri yang dimiliki tumbuhan teratai yaitu ....', 'Mempunyai daun panjang menyiri', ' Mempunyai daun lebar tebal', 'Mempunyai daun berbentuk duri', 'Mempunyai daun tipis dan lebar', 'd'),
(9, 'Hewan-hewan carnivota diatas yang tidak mempunyai gigi yang tajam yaitu ...', 'Buaya', 'Elang', 'Singa', 'Kucing', 'b'),
(10, 'Burung mempunyai paruh dan bentuk kaki yang berbeda-beda sesuai dengan…', 'Jenis makanan dan tempat hidup', 'Bentuk makanannya', 'Keindahan tempatnya', 'Kelincahan mangsanya', 'a');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_soalipa3`
--

CREATE TABLE `tbl_soalipa3` (
  `id_soal` int(5) NOT NULL,
  `soal` text NOT NULL,
  `a` varchar(30) NOT NULL,
  `b` varchar(30) NOT NULL,
  `c` varchar(30) NOT NULL,
  `d` varchar(30) NOT NULL,
  `knc_jawaban` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_soalipa3`
--

INSERT INTO `tbl_soalipa3` (`id_soal`, `soal`, `a`, `b`, `c`, `d`, `knc_jawaban`) VALUES
(1, '  Tumbuhan insektivora merupakan tumbuhan pemakan ....', 'Serangga', 'Mamalia', ' Tumbuhan', 'Cicak', 'a'),
(2, ' Mempunyai daun yang lebar dan batang berongga adalah ciri-ciri penyesuaian diri tumbuhan...', 'Teratai', 'Kaktus', 'Mangga', 'Kembang sepatu', 'a'),
(3, 'Kupu-kupu mempunyai bentuk mulut...', 'Penusuk ', ' Penjilat', 'Penusuk dan penghisap', 'Penghisap', 'd'),
(4, 'Kaktus mempunyai akar yang panjang, untuk..', 'Bernafas', ' Membuat makanan', ' Mecari sumber air yang letakn', 'Menyimpan cadangan makanan', 'c'),
(5, 'Selaput pada kaki bebek bermanfaat untuk...', ' Bertengger di ranting pohon', 'Memenjat pohon', 'Mencengkram mangsanya', 'Berenang di air', 'd'),
(6, ' Ayam mempunyai alat bela diri untuk melukai musuhnya yang disebut ...', 'Mata', 'Jengger', 'Taji', 'Ekor', 'c'),
(7, 'Unta mampu bertahan tanpa minum air dalam waktu lama karena mempunyai....', 'Cadangan lemak yang dapat diub', ' Punuh yang kuat semar', 'Leher panjang', 'Kulit yang diselimuti rambut t', 'a'),
(8, ' Kambing, sapi, dan kerbau mempunyai alat bela diri berupa....', 'Sengat yang berbisa', ' Tanduk yang runcing', ' Ekor yang panjang', 'Duri yang tajam', 'b'),
(9, 'Tumbuhan yang tidak menggugurkan daunnya pada saat kemarau yaitu...', 'Pinus', 'Randu', 'Jati', 'Kedondong', 'd'),
(10, ' Kumbang badak mempunyai alat bela diri berupa..', 'Duri yang tajam', 'Jengger', 'Taji', 'Tanduk yang kuat', 'b');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_soalmtk1`
--

CREATE TABLE `tbl_soalmtk1` (
  `id_soal` int(5) NOT NULL,
  `soal` text NOT NULL,
  `a` varchar(30) NOT NULL,
  `b` varchar(30) NOT NULL,
  `c` varchar(30) NOT NULL,
  `d` varchar(30) NOT NULL,
  `knc_jawaban` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_soalmtk1`
--

INSERT INTO `tbl_soalmtk1` (`id_soal`, `soal`, `a`, `b`, `c`, `d`, `knc_jawaban`) VALUES
(8, 'Dua buah lingkaran dengan jarak kedua pusat lingkaran adalah 26 cm, memiliki jari-jari lingkaran besar 12 cm, dan jari-jari lingkaran kecil 2 cm. Maka, panjang garis singgung persekutuan luarnya adalah ...', '24 cm ', '42 cm', '60 cm', '82 cm', 'a'),
(7, 'Pada layar televisi, panjang sebuah mobil adalah 14 cm dan tingginya 4 cm. Jika tinggi sebenarnya adalah 1 m, maka panjang mobil sebenarnya adalah...', '3 m', '3,5 m', '4 m', '4,5 m', 'b'),
(5, 'Sebuah taman berbentuk persegi. Di sekeliling taman itu ditanami pohon cemara dengan jarak antar pohon 8 m. Panjang sisi taman itu adalah 78 m. Berapakah banyak pohon cemara yang dibutuhkan?', '30 buah\r\n', '35 buah', '39 buah', '49 buah', 'c'),
(4, 'Umur Ibu 4 tahun lebih muda dari umur Ayah. Jumlah umur Ibu dan Ayah adalah 78 tahun. Berapakah umur Ayah sekarang?', '30', '37', '41', '50', 'c'),
(3, 'Diberikan P = {1,2,3,4,5,6,7,8,9}. Himpunan bilangan ganjil yang terdapat di P adalah....', ' {1,2,3,7} ', ' {2,4,8,9}', ' {1,3,5,7,9}', '{1,5,7,9}', 'c'),
(6, 'Sebuah tangki air berbentuk tabung dengan jari-jari 60 cm dan tinggi 1,4 m akan diisi air sampai penuh. Banyaknya air yang mengisi tangki adalah..liter', '1.864 liter', '1.584 liter', '1.684 liter', '1.464 liter', 'b'),
(2, 'Anis membeli sebuah telfon genggam dengan harga Rp1.800.000,00, setelah pemakaian 3 bulan Anis menjual dengan harga Rp1.200.000,00. Berapa persentase kerugian yang dialami oleh Anis?', '23%', '33%', '34%', '35%', 'b'),
(1, 'Perbandingan panjang dan lebar persegi panjang adalah 4 : 3. Jika kelilingnya 84 cm, luasnya adalah..', '234 cm2', '324 cm2', '432 cm2', '452 cm2', 'c'),
(9, 'Sinta memiliki bak mandi berbentuk balok dengan tinggi 30 cm, lebarnya 60 cm dan panjang 100 cm. Bak tersebut akan diisi air. Banyak air yang dibutuhkan Sinta untuk mengiri 2/3 bagian bak mandi tersebut adalah ...', ' 210.000 cm3', '180.000 cm3', '160.000 cm3', '120.000 cm3', 'd'),
(10, 'Alas sebuah prisma berbentuk segitiga siku-siku dengan panjang 12 cm, 16 cm, dan 20 cm. Jika tinggi prisma 30 cm, maka volume prisma tersebut adalah... ', '987 cm3', '1.208 cm3', '2.880 cm3', '3.675 cm3', 'c');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_soalmtk2`
--

CREATE TABLE `tbl_soalmtk2` (
  `id_soal` int(5) NOT NULL,
  `soal` text NOT NULL,
  `a` varchar(30) NOT NULL,
  `b` varchar(30) NOT NULL,
  `c` varchar(30) NOT NULL,
  `d` varchar(30) NOT NULL,
  `knc_jawaban` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_soalmtk2`
--

INSERT INTO `tbl_soalmtk2` (`id_soal`, `soal`, `a`, `b`, `c`, `d`, `knc_jawaban`) VALUES
(1, 'Alas sebuah limas beraturan berbentuk persegi dengan panjang sisi 10 cm dan tinggi limas 12 cm, maka luas permukaan limas adalah...', '260 cm2', '340 cm2', '360 cm2', '620 cm2', 'c'),
(2, 'Pak Komar memiliki sebuah peternakan di mana di dalamnya ada 20 ekor kambing dan 40 ekor sapi. Pak Komar membawa 2 ekor sapi dan 1 ekor kambing untuk diperiksa kesehatannya ke dokter hewan. Dari ilustrasi tersebut, yang merupakan populasi adalah ...', ' Empat puluh ekor sapi', ' Dua puluh ekor kambing', 'Dua ekor sapi dan satu ekor ka', 'Seluruh hewan ternak', 'd'),
(3, 'Dua buah mata uang logam dilempar bersama-sama. Peluang munculnya dua angka adalah...', '0,20 ', '0,25', '0,50', '0,75', 'b'),
(4, 'Apabila a = 3; b = 0; dan c = -3, maka nilai dari  { a x (b + c – a)} x ( b + c ) = ...', ' -54  ', '-45', '45', '54', 'd'),
(5, ' Seorang siswa berhasil menjawab dengan benar 28 soal, salah 8 soal, serta tidak menjawab 4 soal. Bila satu soal dijawab benar nilainya 4, salah nilainya -3, serta tidak menjawab    nilainya  -1. maka nilai yang diperoleh siswa tersebut adalah...', '96  ', '88', '84', '91', 'c'),
(6, 'Jumlah dua bilangan pecahan saling berkebalikan adalah  34/15 . Jika salah satu penyebut bilangan itu adalah 5. salah satu bilangan tersebut adalah...', '2/5', '3/5', '5/4', '5/6', 'b'),
(7, ' Suatu jenis pupuk terdiri dari 50% ammonium, 35% super fosfat, dan sisanya besi sulfat. Jika berat pupuk tersebut 15 kg, maka berat besi sulfat adalah...', ' 22,5 gr ', ' 2.25 g', '2,25 kg', '22,5 kg', 'c'),
(8, 'Siswa SMP Andalan sebanyak 140 anak membentuk suatu barisan. Barisan pertama terdiri dari 5 anak. Dan setiap barisan berikutnya selalu bertambah 2 anak dari barisan sebelumnya. Banyak barisan yang dapat dibentuk oleh seluruh siswa tersebut adalah ... ', '6', '10', '11', '12', 'b'),
(9, 'Jika untuk mengecat dinding yang berukuran 6 m2 dibutuhkan 4 kaleng cat, maka untuk mengecat dinding yang berukuran 15 m2  dibutuhkan kaleng cat sebanyak ...', '10', '9', '8', '7', 'a'),
(10, 'Sebuah bangun ruang berbentuk tabung dengan tutup setengah bola mempunyai diameter 14 cm. Jika tinggi tabung tersebut adalah 20 cm, maka luas permukaan bangun ruang tersebut adalah ...', '1432 cm2', '1342 cm2', '1324 cm2', '1243 cm2', 'b');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_soalmtk3`
--

CREATE TABLE `tbl_soalmtk3` (
  `id_soal` int(5) NOT NULL,
  `soal` text NOT NULL,
  `a` varchar(30) NOT NULL,
  `b` varchar(30) NOT NULL,
  `c` varchar(30) NOT NULL,
  `d` varchar(30) NOT NULL,
  `knc_jawaban` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_soalmtk3`
--

INSERT INTO `tbl_soalmtk3` (`id_soal`, `soal`, `a`, `b`, `c`, `d`, `knc_jawaban`) VALUES
(1, 'Harga beli satu lusin buku kwitansi adalah Rp. 50.000,00 dan dijual dengan harga Rp. 5.000,00 tiap buah. Persentase keuntungannya adalah...', '10%', '12%', '15%', '20%', 'e'),
(2, ' Sebuah koperasi sekolah membeli lima lusin buku seharga Rp. 150.000,00. Jika harga jual sebuah buku Rp. 2.800,00, maka persentase keuntungan yang diperoleh koperasi tersebut adalah...', '4%', '10%', '12%', '6%', 'c'),
(3, ' Toko A memberikan potongan harga 20% pada setiap penjualan barang, untuk pembelian sepasang sepatu, Marliana membayar kepada kasir sebesar Rp. 40.000,00. Harga sepasang sepatu itu sebelum mendapat potongan adalah...', '  Rp.   8.000,00  ', ' Rp. 32.000,00   ', ' Rp. 32.000,00   ', 'Rp. 72.000,00', 'd'),
(4, 'Toko buku sedang memberikan diskon potongan harga 10% pada setiap penjualan barang, untuk pembelian buku matematika, Fulan membayar kepada kasir sebesar Rp. 31.500,00. Harga buku tersebut sebelum mendapat potongan adalah...', 'Rp.   3.500,00', ' Rp. 32.500,00 ', '  Rp. 35.000,00 ', ' Rp. 36.100,00', 'c'),
(5, ' Harga sebuah TV adalah Rp. 586.000,00. Jika terhadap pembelian TV dikenai pajak penjualan sebesar 11%, maka besar uang yang harus dibayar dari pembelian TV tersebut...', ' Rp. 592.446,00', '  Rp. 650.460,00', 'Rp. 651.460,00', 'Rp. 719.290,00', 'b'),
(6, ' Harga dua buku dan dua pensil Rp. 8.800,00. jika harga sebuah buku Rp. 600,00 lebih murah dari harga pensil, maka harga sebuah buku adalah...', 'Rp.1.200 ', 'Rp. 2.500,00', 'Rp.  8.800', ' Rp.  4.800', 'b'),
(7, 'Sebuah koperasi menjual baju seharga  Rp. 864.000,00 setiap lusinnya. Jika hasil penjualan ternyata untung 20% dari harga belinya, maka harga beli sebuah baju adalah...', 'Rp. 14.000,00', 'Rp. 60.000,00', ' Rp. 74.400,00', 'Rp. 720.000,00', 'b'),
(8, 'Seorang pedagang buah membeli 5 kotak jeruk yang tiap kotaknya berisi 5kg seharga Rp. 600.000,00. Jika kemudian jeruk tersebut dijual seharga Rp. 9.000,00 tiap kilogramnya, maka persentase keuntungan yang diperoleh pedagang tersebut adalah...', '7,5%', '8%', '10%', '12,5%', 'd'),
(9, ' Jarak pada peta antara kota Jakarta dan kota Bogor adalah 5 cm, sedangkan jarak yang sesungguhnya 40 km. Skala peta itu adalah...', '  1: 800  ', '1: 8.000.000', ' 1: 800.000', '1: 80.000 ', 'c'),
(10, 'Setelah mendapat bonus 10% seorang karyawan gajinya Rp. 12.100.000,00. Maka gaji sebelum bonus adalah...', 'Rp.1.210.000,00  ', 'Rp.11.000.000,00', ' Rp.10.850.000,00', ' Rp.10.500.000,00  ', 'b');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `level` int(11) NOT NULL,
  `password` varchar(33) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `level`, `password`) VALUES
(1, 'admin', 1, 'c93ccd78b2076528346216b3b2f701e6'),
(2, 'guru', 2, 'b47c6e71ca3a5e23cab99c2e9da03046'),
(3, 'siswa', 3, '1f5cc082506d0121b0fb53ae605ef9e9'),
(4, 'orangtua', 4, 'dea701ae0239b409af97c2ed949ec66c'),
(9, 'ignatius', 1, '11dee9e7cfe66aa660841a8641c407db');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dusun`
--
ALTER TABLE `dusun`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nilai`
--
ALTER TABLE `nilai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_soalbhsindo1`
--
ALTER TABLE `tbl_soalbhsindo1`
  ADD PRIMARY KEY (`id_soal`);

--
-- Indexes for table `tbl_soalbhsindo2`
--
ALTER TABLE `tbl_soalbhsindo2`
  ADD PRIMARY KEY (`id_soal`);

--
-- Indexes for table `tbl_soalbhsindo3`
--
ALTER TABLE `tbl_soalbhsindo3`
  ADD PRIMARY KEY (`id_soal`);

--
-- Indexes for table `tbl_soalipa1`
--
ALTER TABLE `tbl_soalipa1`
  ADD PRIMARY KEY (`id_soal`);

--
-- Indexes for table `tbl_soalipa2`
--
ALTER TABLE `tbl_soalipa2`
  ADD PRIMARY KEY (`id_soal`);

--
-- Indexes for table `tbl_soalipa3`
--
ALTER TABLE `tbl_soalipa3`
  ADD PRIMARY KEY (`id_soal`);

--
-- Indexes for table `tbl_soalmtk1`
--
ALTER TABLE `tbl_soalmtk1`
  ADD PRIMARY KEY (`id_soal`);

--
-- Indexes for table `tbl_soalmtk2`
--
ALTER TABLE `tbl_soalmtk2`
  ADD PRIMARY KEY (`id_soal`);

--
-- Indexes for table `tbl_soalmtk3`
--
ALTER TABLE `tbl_soalmtk3`
  ADD PRIMARY KEY (`id_soal`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dusun`
--
ALTER TABLE `dusun`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `nilai`
--
ALTER TABLE `nilai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_soalbhsindo1`
--
ALTER TABLE `tbl_soalbhsindo1`
  MODIFY `id_soal` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `tbl_soalbhsindo2`
--
ALTER TABLE `tbl_soalbhsindo2`
  MODIFY `id_soal` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `tbl_soalbhsindo3`
--
ALTER TABLE `tbl_soalbhsindo3`
  MODIFY `id_soal` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `tbl_soalipa1`
--
ALTER TABLE `tbl_soalipa1`
  MODIFY `id_soal` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `tbl_soalipa2`
--
ALTER TABLE `tbl_soalipa2`
  MODIFY `id_soal` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `tbl_soalipa3`
--
ALTER TABLE `tbl_soalipa3`
  MODIFY `id_soal` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `tbl_soalmtk1`
--
ALTER TABLE `tbl_soalmtk1`
  MODIFY `id_soal` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `tbl_soalmtk2`
--
ALTER TABLE `tbl_soalmtk2`
  MODIFY `id_soal` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `tbl_soalmtk3`
--
ALTER TABLE `tbl_soalmtk3`
  MODIFY `id_soal` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
