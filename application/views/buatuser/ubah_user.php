<!DOCTYPE html>

<html>

<head>

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="<?php echo base_url(); ?>template/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>template/css/style.css" rel="stylesheet">

</head>

<body>



<div class="container">

   <h3>Ubah Data User</h3>

  <div class="table-responsive">

  <table class="table">

    <thead>

      <tr>

        <th></th>

        <th></th>

      </tr>

    </thead>

    <tbody>

      <form action="<?php echo base_url().'index.php/buatuseradmin/update';?>" method="post">

        <?php

        foreach ($data as $key => $value) { ?>

          <tr>

            <td width="100">username</td>

            <td width="300">

              <input type="hidden" class="form-control" value="<?php echo $value->id;?>" name="id" placeholder="username">

              <input type="text" class="form-control" value="<?php echo $value->username;?>" name="username" placeholder="username">

            </td> </tr>
        <tr>

            <td width="100">level</td>

            <td width="300">

              <input type="text" class="form-control" value="<?php echo $value->level;?>" name="level" placeholder="level">

            </td>

          </tr>

          <tr>

            <td width="100">password</td>

            <td width="300">

              <input type="text" class="form-control" value="<?php echo $value->password;?>" name="password" placeholder="password">

            </td>

          </tr>

        <?php }

        ?>

          <tr>

            <td colspan="2">

              <a href="<?php echo base_url().'index.php/buatuseradmin/';?>" class="btn btn-info">Batal</a>

              <button type="submit" class="btn btn-primary">Simpan</button>

            </td>

          </tr>

    </form>

    </tbody>

  </table>

  </div>

</div>



</body>

</html>