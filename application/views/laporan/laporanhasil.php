<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="utf-8">

 <style type="text/css">
 body {
 background-color: gold;
 font-family: Arial;
 }
 main {
 width: 70%;
 padding: 70px;
 background-color: white;
 min-height: 300px;
 border-radius: 5px;
 margin: 60px auto;
 }
 table {
 border-top: solid thin #000;
 border-collapse: collapse;
 }
 th, td {
 border-top: border-top: solid thin #000;
 padding: 6px 12px;
 }
 </style>
</head>

<body>
  <center>
 <main>
 <font face="castellar" size="6">Laporan Hasil Nilai</font>



<div class="container">

  

  <a href="<?php echo base_url().'index.php/laporanhasiladmin/tambah';?>" class="btn btn-primary">Tambah</a>
<a href="<?php echo base_url().'index.php/kembali/admin ';?>" class="btn btn-primary">Kembali</a>

  
  <div class="table-responsive">

  <table class="table">
<br>
    <thead>

      <tr>

        <th>id</th>

        <th>username</th>

        <th>nilai_bahasa_indonesia</th>

        <th>nilai_matematika</th>

        <th>nilai_ipa</th>

    <th>



    </th>

      </tr>

    </thead>

    <tbody>



    <?php

    $i = 1;

    foreach ($data as $key => $value) {

     ?>

     <tr>

      <td><?php echo $i;?></td>

          <td><?php echo $value->username;?></td>

          <td><?php echo $value->nilai_bahasa_indonesia;?></td>

          <td><?php echo $value->nilai_matematika;?></td>

          <td><?php echo $value->nilai_ipa;?></td>

      <td>



        <a href="<?php echo base_url().'index.php/laporanhasiladmin/hapus/'.$value->id;?>" class="btn btn-danger">Hapus</a>

      </td>

     </tr>

     <?php

     $i++;

    }

    ?>

    </tbody>

  </table>

  </div>

</div>


 </main>
 </center>
</body>
</html>