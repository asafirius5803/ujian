<!DOCTYPE html>
<html lang="">
	<head> <title>Aplikasi Ujian Online</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Aplikasi Ujian Online</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	</head>
	<body>


		<div class="container"><br>
	
		        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                
               <div class="login-panel panel panel-default">
                    
                    <center>
                   
                    </center>
                    <div class="panel-heading"><center><h4>Selamat Datang di Aplikasi Ujian Online</h4>
                       <h4>Silahkan Login</h4></center>
                    </div>
                    <div class="panel-body">
                    	 <div id="loading" style="text-align: center"></div>
			<form action="<?php echo base_url('index.php/login/user')?>" method="POST" >
				
					<fieldset>
				<div class="form-group has-feedback">
					<label>Username</label>  <i class="fa fa-user"></i>
					<input type="text" class="form-control"  name="username" placeholder="username">
				</div>
			
				<div class="form-group">
					<label>Password</label>  <i class="fa fa-lock"></i>

					<input type="password" class="form-control"  name="password" placeholder="password">				</div>
					<div class="checkbox">
                       <label>Belum punya akun ? daftar <a href="<?php echo base_url('index.php/buatuseradmin/register')?>">disini</a></label></div>
                                    
				
				     <button type="submit" class="btn btn-lg btn-success btn-block" value="Login">Sign in</button>
				
				</fieldset>

			</form>
		</div>
	</div>
</div>
</div>


    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="js/jquery.validate.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>


	</body>
</html>