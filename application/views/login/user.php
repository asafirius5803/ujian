<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title> Login </title>
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
	</head>
	<body>
		<div class="container">
			<h3></h3>
			<hr>
<?php 
if(count($login)>0)
{
	if($login->level == 1)
	{
		$this->load->view("halaman/halaman_admin");
 
	}
 
	elseif($login->level == 2)
	{
			$this->load->view("halaman/halaman_guru");
	}
 
	elseif($login->level == 3)
	{
				$this->load->view("halaman/halaman_siswa");
	}
 
	elseif($login->level == 4)
	{
				$this->load->view("halaman/halaman_orangtua");
	}
	else
	{
		echo "<p>Anda login sebagai <b>". $login->username . "</b> .Level belum di setting, kontak admin.</p>";
	}
}
else
{
 echo '<script language="javascript">
	alert("Userid atau Password Yang anda Masukkan Salah atau Acount Sudah Diblokir");
	window.location="/";
	</script>';
	exit();
}

?>
		</div>
	</body>
</html>