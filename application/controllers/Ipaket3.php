<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Ipaket3 extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('ipaket3model');

  $this->load->helper('url');

 }



 public function index()

 {


  $result ['data'] = $this->ipaket3model->get_data();


  $this->load->view('soal/ipaket3tampil', $result);

 }



 public function tambah(){

  $this->load->view('soal/ipaket3tambah');

 }

public function save(){



  $datapos = $this->input->post();



  $data = $this->ipaket3model->save_data($datapos);





  redirect( base_url() . 'index.php/ipaket3');

 }



 function ubah(){

  $id = $this->uri->segment(3);

  $data = $this->ipaket3model->get_dataByid($id);

  $result['data'] = $data;

  $this->load->view('soal/ipaket3ubah', $result);

 }



 public function update(){

  $data = $this->input->post();

  $data = $this->ipaket3model->update_data($data);

  redirect( base_url() . 'index.php/ipaket3');

 }



 function hapus(){

  $id = $this->uri->segment(3);



  $query = $this->ipaket3model->hapus_data($id);

  redirect( base_url() . 'index.php/ipaket3');

 }



}
