<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Ipaket2guru extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('ipaket2gurumodel');

  $this->load->helper('url');

 }



 public function index()

 {


  $result ['data'] = $this->ipaket2gurumodel->get_data();


  $this->load->view('soal/ipaket2gurutampil', $result);

 }



}
