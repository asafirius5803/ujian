<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporanhasillihat extends CI_Controller {


 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('laporan_model');

  $this->load->helper('url');

 }



 public function index()

 {


  $result ['data'] = $this->laporan_model->get_data();


  $this->load->view('laporan/laporanlihat', $result);

 }



}