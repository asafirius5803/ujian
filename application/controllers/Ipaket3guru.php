<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Ipaket3guru extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('ipaket3gurumodel');

  $this->load->helper('url');

 }



 public function index()

 {


  $result ['data'] = $this->ipaket3gurumodel->get_data();


  $this->load->view('soal/ipaket3gurutampil', $result);

 }



}
