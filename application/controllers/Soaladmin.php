<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Soaladmin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	 $this->CI = & get_instance();

	}
	public function index()
	{

		$this->load->view("soal/soal");

	}
	public function matematika()
	{
		$this->load->view("soal/matematikatampil");
    }
	public function bahasaindonesia()
	{
		$this->load->view("soal/bahasaindonesiatampil");
    }
	public function ipa()
	{
		$this->load->view("soal/ipatampil");
    }

}