<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Bpaket1guru extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('bpaket1gurumodel');

  $this->load->helper('url');

 }



 public function index()

 {


  $result ['data'] = $this->bpaket1gurumodel->get_data();


  $this->load->view('soal/bpaket1gurutampil', $result);

 }



}
