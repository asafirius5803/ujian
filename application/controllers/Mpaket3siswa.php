<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Mpaket3siswa extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('mpaket3siswamodel');

  $this->load->helper('url');

 }



 public function index()

 {
 

  $result ['data'] = $this->mpaket3siswamodel->get_data();


  $this->load->view('soal/mpaket3siswatampil', $result);

 }



}
