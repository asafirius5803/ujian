<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Mpaket2 extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('mpaket2model');

  $this->load->helper('url');

 }



 public function index()

 {


  $result ['data'] = $this->mpaket2model->get_data();


  $this->load->view('soal/mpaket2tampil', $result);

 }



 public function tambah(){

  $this->load->view('soal/mpaket2tambah');

 }

public function save(){



  $datapos = $this->input->post();



  $data = $this->mpaket2model->save_data($datapos);





  redirect( base_url() . 'index.php/mpaket2');

 }



 function ubah(){

  $id = $this->uri->segment(3);

  $data = $this->mpaket2model->get_dataByid($id);

  $result['data'] = $data;

  $this->load->view('soal/mpaket2ubah', $result);

 }



 public function update(){

  $data = $this->input->post();

  $data = $this->mpaket2model->update_data($data);

  redirect( base_url() . 'index.php/mpaket2');

 }



 function hapus(){

  $id = $this->uri->segment(3);



  $query = $this->mpaket2model->hapus_data($id);

  redirect( base_url() . 'index.php/mpaket2');

 }



}
