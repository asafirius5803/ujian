<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Bpaket3guru extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('bpaket3gurumodel');

  $this->load->helper('url');

 }



 public function index()

 {


  $result ['data'] = $this->bpaket3gurumodel->get_data();


  $this->load->view('soal/bpaket3gurutampil', $result);

 }



}
