<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Soalsiswa extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	 $this->CI = & get_instance();

	}
	public function index()
	{

		$this->load->view("soal/soalsiswa");

	}
	public function matematika()
	{
		$this->load->view("soal/matematikasiswatampil");
    }
	public function bahasaindonesia()
	{
		$this->load->view("soal/bahasaindonesiasiswatampil");
    }
	public function ipa()
	{
		$this->load->view("soal/ipasiswatampil");
    }
    	
}