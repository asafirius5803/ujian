<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Mpaket2guru extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('mpaket2gurumodel');

  $this->load->helper('url');

 }



 public function index()

 {


  $result ['data'] = $this->mpaket2gurumodel->get_data();


  $this->load->view('soal/mpaket2gurutampil', $result);

 }



}
