<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Bpaket1 extends CI_Controller {

 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('bpaket1model');

  $this->load->helper('url');

 }



 public function index()

 {


  $result ['data'] = $this->bpaket1model->get_data();


  $this->load->view('soal/bpaket1tampil', $result);

 }



 public function tambah(){

  $this->load->view('soal/bpaket1tambah');

 }

public function save(){



  $datapos = $this->input->post();



  $data = $this->bpaket1model->save_data($datapos);





  redirect( base_url() . 'index.php/bpaket1');

 }



 function ubah(){

  $id = $this->uri->segment(3);

  $data = $this->bpaket1model->get_dataByid($id);

  $result['data'] = $data;

  $this->load->view('soal/bpaket1ubah', $result);

 }



 public function update(){

  $data = $this->input->post();

  $data = $this->bpaket1model->update_data($data);

  redirect( base_url() . 'index.php/bpaket1');

 }



 function hapus(){

  $id = $this->uri->segment(3);



  $query = $this->bpaket1model->hapus_data($id);

  redirect( base_url() . 'index.php/bpaket1');

 }



}
