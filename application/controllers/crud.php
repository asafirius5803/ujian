<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Crud extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('crudmodel');

  $this->load->helper('url');

 }



 public function index()

 {


  $result ['data'] = $this->crudmodel->get_data();


  $this->load->view('crud_tampil', $result);

 }



 public function tambah(){

  $this->load->view('tambah_data');

 }



 public function save(){

  $datapos = $this->input->post();

  $data = $this->crudmodel->save_data($datapos);



  redirect( base_url() . 'index.php/crud');

 }



 function ubah(){

  $id = $this->uri->segment(3);

  $data = $this->crudmodel->get_dataByid($id);

  $result['data'] = $data;

  $this->load->view('ubah_data', $result);

 }



 public function update(){

  $datapos = $this->input->post();

  $data = $this->crudmodel->update_data($datapos);

  redirect( base_url() . 'index.php/crud');

 }



 function hapus(){

  $id = $this->uri->segment(3);



  $query = $this->crudmodel->hapus_data($id);

  redirect( base_url() . 'index.php/crud');

 }



}
