<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Ipaket2 extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('ipaket2model');

  $this->load->helper('url');

 }



 public function index()

 {


  $result ['data'] = $this->ipaket2model->get_data();


  $this->load->view('soal/ipaket2tampil', $result);

 }



 public function tambah(){

  $this->load->view('soal/ipaket2tambah');

 }

public function save(){



  $datapos = $this->input->post();



  $data = $this->ipaket2model->save_data($datapos);





  redirect( base_url() . 'index.php/ipaket2');

 }



 function ubah(){

  $id = $this->uri->segment(3);

  $data = $this->ipaket2model->get_dataByid($id);

  $result['data'] = $data;

  $this->load->view('soal/ipaket2ubah', $result);

 }



 public function update(){

  $data = $this->input->post();

  $data = $this->ipaket2model->update_data($data);

  redirect( base_url() . 'index.php/ipaket2');

 }



 function hapus(){

  $id = $this->uri->segment(3);



  $query = $this->ipaket2model->hapus_data($id);

  redirect( base_url() . 'index.php/ipaket2');

 }



}
