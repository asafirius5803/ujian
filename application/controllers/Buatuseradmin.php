<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Buatuseradmin extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('buatusermodel');

  $this->load->helper('url');

 }



 public function index()

 {


  $result ['data'] = $this->buatusermodel->get_data();


  $this->load->view('buatuser/buatuser_tampil', $result);

 }



 public function tambah(){

  $this->load->view('buatuser/tambah_user');

 }



 public function save(){

  $datapos = $this->input->post(); /*$_POST[]*/

  // var_dump($datapos); die('aa');

  $data = $this->buatusermodel->save_data($datapos);


  redirect( base_url() . 'index.php/Buatuseradmin');

 }



 function ubah(){

  $id = $this->uri->segment(3);

  $data = $this->buatusermodel->get_dataByid($id);

  $result['data'] = $data;

  $this->load->view('buatuser/ubah_user', $result);

 }



 public function update(){

  $datapos = $this->input->post();

  $data = $this->buatusermodel->update_data($datapos);

  redirect( base_url() . 'index.php/Buatuseradmin');

 }



 function hapus(){

  $id = $this->uri->segment(3);



  $query = $this->buatusermodel->hapus_user($id);

  redirect( base_url() . 'index.php/Buatuseradmin');

 }


 public function register(){

  $this->load->view('buatuser/register');

 }

 public function saveregister(){

  $datapos = $this->input->post(); /*$_POST[]*/

  // var_dump($datapos); die('aa');

  $data = $this->buatusermodel->save_data($datapos);


  redirect( base_url() . 'login');

 }





}
