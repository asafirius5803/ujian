<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Ipaket3siswa extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('ipaket3siswamodel');

  $this->load->helper('url');

 }



 public function index()

 {
 

  $result ['data'] = $this->ipaket3siswamodel->get_data();


  $this->load->view('soal/ipaket3siswatampil', $result);

 }



}
