<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Jawab extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('jawabmodel');

  $this->load->helper('url');

 }


 public function save(){

  $datapos = $this->input->post(); 
  $data = $this->jawabmodel->save_data($datapos);


  redirect( base_url() . 'index.php/soalsiswa/bahasaindonesia/');

 }


 public function jawab()

 { 

  $result ['data'] = $this->jawabmodel->get_data();

  $this->load->view('soal/jawab',$result);

 }
}