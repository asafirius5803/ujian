<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Bpaket2siswa extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('bpaket2siswamodel');

  $this->load->helper('url');

 }



 public function index()

 {


  $result ['data'] = $this->bpaket2siswamodel->get_data();


  $this->load->view('soal/bpaket2siswatampil', $result);

 }



}
