<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Ipaket1 extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('ipaket1model');

  $this->load->helper('url');

 }



 public function index()

 {


  $result ['data'] = $this->ipaket1model->get_data();


  $this->load->view('soal/ipaket1tampil', $result);

 }



 public function tambah(){

  $this->load->view('soal/ipaket1tambah');

 }

public function save(){



  $datapos = $this->input->post();



  $data = $this->ipaket1model->save_data($datapos);





  redirect( base_url() . 'index.php/ipaket1');

 }



 function ubah(){

  $id = $this->uri->segment(3);

  $data = $this->ipaket1model->get_dataByid($id);

  $result['data'] = $data;

  $this->load->view('soal/ipaket1ubah', $result);

 }



 public function update(){

  $data = $this->input->post();

  $data = $this->ipaket1model->update_data($data);

  redirect( base_url() . 'index.php/ipaket1');

 }



 function hapus(){

  $id = $this->uri->segment(3);



  $query = $this->ipaket1model->hapus_data($id);

  redirect( base_url() . 'index.php/ipaket1');

 }



}
