<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporanhasiladmin extends CI_Controller {


 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('laporan_model');

  $this->load->helper('url');

 }



 public function index()

 {


  $result ['data'] = $this->laporan_model->get_data();


  $this->load->view('laporan/laporanhasil', $result);

 }



 public function tambah(){

  $this->load->view('laporan/tambahlaporan');

 }



 public function save(){

  $datapos = $this->input->post();

  $data = $this->laporan_model->save_data($datapos);


  redirect( base_url() . 'index.php/laporanhasiladmin');

 }



 public function update(){

  $datapos = $this->input->post();

  $data = $this->laporan_model->update_data($datapos);

  redirect( base_url() . 'index.php/laporanhasiladmin');

 }



 function hapus(){

  $id = $this->uri->segment(3);



  $query = $this->laporan_model->hapus_user($id);

  redirect( base_url() . 'index.php/laporanhasiladmin');

 }



}