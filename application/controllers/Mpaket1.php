<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Mpaket1 extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('mpaket1model');

  $this->load->helper('url');

 }



 public function index()

 {


  $result ['data'] = $this->mpaket1model->get_data();


  $this->load->view('soal/mpaket1tampil', $result);

 }



 public function tambah(){

  $this->load->view('soal/mpaket1tambah');

 }

public function save(){



  $datapos = $this->input->post();



  $data = $this->mpaket1model->save_data($datapos);





  redirect( base_url() . 'index.php/mpaket1');

 }



 function ubah(){

  $id = $this->uri->segment(3);

  $data = $this->mpaket1model->get_dataByid($id);

  $result['data'] = $data;

  $this->load->view('soal/mpaket1ubah', $result);

 }



 public function update(){

  $data = $this->input->post();

  $data = $this->mpaket1model->update_data($data);

  redirect( base_url() . 'index.php/mpaket1');

 }



 function hapus(){

  $id = $this->uri->segment(3);



  $query = $this->mpaket1model->hapus_data($id);

  redirect( base_url() . 'index.php/mpaket1');

 }



}
