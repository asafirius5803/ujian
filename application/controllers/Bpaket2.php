<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Bpaket2 extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('bpaket2model');

  $this->load->helper('url');

 }



 public function index()

 {


  $result ['data'] = $this->bpaket2model->get_data();


  $this->load->view('soal/bpaket2tampil', $result);

 }



 public function tambah(){

  $this->load->view('soal/bpaket2tambah');

 }

public function save(){



  $datapos = $this->input->post();



  $data = $this->bpaket2model->save_data($datapos);





  redirect( base_url() . 'index.php/bpaket2');

 }



 function ubah(){

  $id = $this->uri->segment(3);

  $data = $this->bpaket2model->get_dataByid($id);

  $result['data'] = $data;

  $this->load->view('soal/bpaket2ubah', $result);

 }



 public function update(){

  $data = $this->input->post();

  $data = $this->bpaket2model->update_data($data);

  redirect( base_url() . 'index.php/bpaket2');

 }



 function hapus(){

  $id = $this->uri->segment(3);



  $query = $this->bpaket2model->hapus_data($id);

  redirect( base_url() . 'index.php/bpaket2');

 }



}
