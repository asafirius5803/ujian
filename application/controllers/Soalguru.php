<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Soalguru extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	 $this->CI = & get_instance();

	}
	public function index()
	{

		$this->load->view("soal/soalguru");

	}
	public function matematika()
	{
		$this->load->view("soal/matematikagurutampil");
    }
	public function bahasaindonesia()
	{
		$this->load->view("soal/bahasaindonesiagurutampil");
    }
	public function ipa()
	{
		$this->load->view("soal/ipagurutampil");
    }

}