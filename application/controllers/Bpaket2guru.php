<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Bpaket2guru extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('bpaket2gurumodel');

  $this->load->helper('url');

 }



 public function index()

 {


  $result ['data'] = $this->bpaket2gurumodel->get_data();


  $this->load->view('soal/bpaket2gurutampil', $result);

 }



}
