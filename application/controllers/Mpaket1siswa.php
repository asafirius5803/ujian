<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Mpaket1siswa extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('mpaket1siswamodel');

  $this->load->helper('url');

 }



 public function index()

 {
 

  $result ['data'] = $this->mpaket1siswamodel->get_data();


  $this->load->view('soal/mpaket1siswatampil', $result);

 }



}
