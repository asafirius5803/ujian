<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Ipaket2siswa extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('ipaket2siswamodel');

  $this->load->helper('url');

 }



 public function index()

 {
 

  $result ['data'] = $this->ipaket2siswamodel->get_data();


  $this->load->view('soal/ipaket2siswatampil', $result);

 }



}
